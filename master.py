from datetime import datetime, timedelta
from json import dumps
from json import load as json_load
from sys import argv
from typing import Dict, Union

import matplotlib.patches as mpatches
import matplotlib.pyplot as plt
import numpy as np
from dateutil.parser import parse as dateparser
from scipy import stats

import pulselabs


def analyse(
    data: np.ndarray, settings: Dict[str, int]
) -> Dict[str, Union[np.ndarray, float]]:
    results: Dict[str, Union[np.ndarray, float]] = {}

    mean_raw: float = data[:, 1].mean()
    median_raw: float = np.median(data[:, 1])
    mode_raw: float = stats.mode(data[:, 1])[0][0]
    mean: float = 0
    median: float = 0
    mode: float = 0
    tolerance_limit_upper_raw: float = median_raw * (1 + settings["tolerance_upper"])
    tolerance_limit_lower_raw: float = median_raw * (1 - settings["tolerance_lower"])
    tolerance_limit_upper: float = 0
    tolerance_limit_lower: float = 0
    minimum: float = data[:, 1].min()
    maximum: float = data[:, 1].max()
    range_unit: float = (maximum - minimum) / 100
    oldest: float = data[:, 0].min()
    newest: float = data[:, 0].max()
    duration: float = newest - oldest

    data_above: np.ndarray = np.empty(shape=(0, 2))
    data_below: np.ndarray = np.empty(shape=(0, 2))
    data_inside: np.ndarray = np.empty(shape=(0, 2))
    data_outlier: np.ndarray = np.empty(shape=(0, 2))
    data_outlier_added: list = []
    data_outlier_ranges: np.ndarray = np.empty(shape=(0, 2))

    datapoint: np.ndarray
    outliers_threshold: list = [1]
    median_adjusted: float = median_raw
    tolerance_limit_upper_adjusted: float = tolerance_limit_upper_raw
    tolerance_limit_lower_adjusted: float = tolerance_limit_lower_raw
    temp_data: np.ndarray = data
    while len(outliers_threshold):
        outliers_threshold = []

        for i, datapoint in enumerate(temp_data):
            if (
                datapoint[1] > tolerance_limit_upper_adjusted
                or datapoint[1] < tolerance_limit_lower_adjusted
            ):
                outliers_threshold.append(i)

        if not outliers_threshold:
            break

        for d in outliers_threshold:
            temp_data = np.vstack((temp_data[:d], temp_data[d + 1 :]))

        median_adjusted = np.median(temp_data[:, 1])
        tolerance_limit_upper_adjusted = median_adjusted * (
            1 + settings["tolerance_upper"]
        )
        tolerance_limit_lower_adjusted = median_adjusted * (
            1 - settings["tolerance_lower"]
        )

    mean = temp_data[:, 1].mean()
    median = median_adjusted
    mode = stats.mode(temp_data[:, 1])[0][0]
    tolerance_limit_upper = tolerance_limit_upper_adjusted
    tolerance_limit_lower = tolerance_limit_lower_adjusted

    for datapoint in data:
        if datapoint[1] > tolerance_limit_upper:
            data_above = np.vstack((data_above, datapoint))
        elif datapoint[1] < tolerance_limit_lower:
            data_below = np.vstack((data_below, datapoint))
        else:
            data_inside = np.vstack((data_inside, datapoint))

    current_range: np.ndarray = np.array([0, 0])

    for i in range(
        settings["scan_outlier_size"], len(data) - settings["scan_outlier_size"]
    ):
        outliers: list = []

        for j in range(
            i - settings["scan_outlier_size"], i + settings["scan_outlier_size"] + 1
        ):
            if data[j][1] < tolerance_limit_lower or data[j][1] > tolerance_limit_upper:
                outliers.append(j)

        if len(outliers) > settings["scan_outlier_threshold"]:
            current_range[0] = outliers[0] if not current_range[0] else current_range[0]
            current_range[1] = outliers[-1]
            for outlier in outliers:
                if outlier not in data_outlier_added:
                    data_outlier = np.vstack((data_outlier, data[outlier]))
                    data_outlier_added.append(outlier)
        else:
            if current_range[0]:
                current_range[0] = data[current_range[0]][0]
                current_range[1] = data[current_range[1]][0]
                if len(data_outlier_ranges):
                    if (
                        current_range[0] >= data_outlier_ranges[-1][0]
                        and current_range[0] <= data_outlier_ranges[-1][1]
                    ):
                        if current_range[1] > data_outlier_ranges[-1][1]:
                            data_outlier_ranges[-1][1] = current_range[1]
                        current_range[0] = 0

                if current_range[0]:
                    data_outlier_ranges = np.vstack(
                        (data_outlier_ranges, current_range)
                    )
                current_range = np.array([0, 0])

    if current_range[0]:
        current_range[0] = data[current_range[0]][0]
        current_range[1] = data[current_range[1]][0]
        if len(data_outlier_ranges):
            if (
                current_range[0] >= data_outlier_ranges[-1][0]
                and current_range[0] <= data_outlier_ranges[-1][1]
            ):
                if current_range[1] > data_outlier_ranges[-1][1]:
                    data_outlier_ranges[-1][1] = current_range[1]
                current_range[0] = 0

        if current_range[0]:
            data_outlier_ranges = np.vstack((data_outlier_ranges, current_range))
        current_range = np.array([0, 0])

    results["count"] = len(data)
    results["mean"] = mean
    results["median"] = median
    results["mode"] = mode
    results["mean_raw"] = mean_raw
    results["median_raw"] = median_raw
    results["mode_raw"] = mode_raw
    results["minimum"] = minimum
    results["maximum"] = maximum
    results["range_unit"] = range_unit
    results["tolerance_upper"] = settings["tolerance_upper"]
    results["tolerance_lower"] = settings["tolerance_lower"]
    results["tolerance_limit_upper"] = tolerance_limit_upper
    results["tolerance_limit_lower"] = tolerance_limit_lower
    results["tolerance_limit_upper_raw"] = tolerance_limit_upper_raw
    results["tolerance_limit_lower_raw"] = tolerance_limit_lower_raw
    results["oldest"] = oldest
    results["newest"] = newest
    results["duration"] = duration
    results["data_above"] = data_above
    results["data_below"] = data_below
    results["data_inside"] = data_inside
    results["data_outlier"] = data_outlier
    results["data_outlier_ranges"] = data_outlier_ranges

    return results


def plot(
    data: np.ndarray, results: Dict[str, Union[np.ndarray, float]], title: str = ""
):
    times: list = [datetime.fromtimestamp(t) for t in data[:, 0]]
    values: list = data[:, 1].tolist()
    handles: list = []

    if title:
        plt.title(title)

    plt.plot(times, values)
    plt.xlabel("Time")
    plt.ylabel("Values")

    handles.append(
        plt.axhline(
            results["mean"],
            linestyle=":",
            color="black",
            label=f"Mean {results['mean']:.3e}",
        )
    )
    handles.append(
        plt.axhline(
            results["mode"],
            linestyle=":",
            color="purple",
            label=f"Mode {results['mode']:.3e}",
        )
    )
    handles.append(
        plt.axhline(
            results["median"],
            linestyle=":",
            color="blue",
            label=f"Median {results['median']:.3e}",
        )
    )
    handles.append(
        plt.axhline(
            results["minimum"],
            linestyle="--",
            color="red",
            label=f"Minimum {results['minimum']:.3e}",
        )
    )
    handles.append(
        plt.axhline(
            results["maximum"],
            linestyle="--",
            color="red",
            label=f"Maximum {results['maximum']:.3e}",
        )
    )
    handles.append(
        plt.axhspan(
            results["tolerance_limit_lower"],
            results["tolerance_limit_upper"],
            color="green",
            alpha=0.5,
            label=f"Tolerance range [{results['tolerance_limit_lower']:.3e}, {results['tolerance_limit_upper']:.3e}]",
        )
    )

    data_outlier_ranges: np.array = results["data_outlier_ranges"]
    if len(data_outlier_ranges):
        outlier_range: np.ndarray
        for outlier_range in data_outlier_ranges:
            plt.axvspan(
                datetime.fromtimestamp(outlier_range[0]),
                datetime.fromtimestamp(outlier_range[1]),
                color="yellow",
                alpha=0.5,
            )

        handles.append(
            mpatches.Patch(
                color="yellow",
                alpha=0.5,
                label=f"Outlying events {len(data_outlier_ranges)}",
            )
        )

    plt.legend(handles=handles)

    plt.show()


def main(origin_settings: dict, analysis_settings: dict, doplot: bool = False):
    # Get credentials & init api
    creds: Dict[str, str]
    with open("credentials.json", "r") as f:
        creds = json_load(f)

    pulseapi = pulselabs.pulselabsAPI(
        creds.get("username", creds.get("email", "")), creds.get("password", "")
    )

    __, code = pulseapi.authenticate()

    if code != 200 or not pulseapi.token:
        raise pulselabs.AuthenticationError(code)
    else:
        print("Logged in")

    # Set interval & truncate (micro)seconds
    date_end: datetime = datetime.utcnow()
    date_beg: datetime = date_end - timedelta(**origin_settings["interval"])

    # Get raw data for interval
    data_csv, code = pulseapi.get_data_csv(
        origin_settings["device"],
        origin_settings["sensor"],
        fromDate=date_beg.isoformat(),
        toDate=date_end.isoformat(),
    )

    data: np.ndarray = np.array(
        sorted(
            [
                [dateparser(d.split(";")[0]).timestamp(), float(d.split(";")[1])]
                for d in data_csv.split("\r\n")
                if d and len(d.split(";")) == 2
            ],
            key=(lambda x: x[0]),
        )
    )

    if code != 200:
        raise pulselabs.APIError(code)

    print(f"Downloaded {len(data)} datapoints")

    if not len(data):
        return

    analysis_results: Dict[str, Union[np.ndarray, float]] = analyse(
        data, analysis_settings
    )

    np.set_printoptions(threshold=12, edgeitems=5, suppress=True)
    for k, v in analysis_results.items():
        print(k + ":", end=" ")
        if isinstance(v, np.ndarray):
            print(f"[{len(v)}]\n", v)
        elif isinstance(v, float):
            print(f"{v:.3e}")
        elif isinstance(v, int):
            print(f"{v:.0e}")
        else:
            print(v)

    if doplot:
        plot(data, analysis_results, str(date_end - date_beg))


if __name__ == "__main__":
    # Options for data origin
    origin_settings: dict = {
        "device": 232,
        "sensor": 1,
        "interval": {"hours": 24 if not argv[1:] else int(argv[1])},
    }

    # Options for data analysis
    analysis_settings: Dict[str, float] = {
        "tolerance_upper": 0.5 / 100,  # amount *above* average considered acceptable
        "tolerance_lower": 0.5 / 100,  # amount *below* average considered acceptable
        "scan_outlier_size": 8,  # The amount from the pointer to search for consitent outliers
        "scan_outlier_threshold": 3,  # The amount of outliers which trigger a true identification
    }

    main(origin_settings, analysis_settings, True)
